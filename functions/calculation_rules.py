"""
File consists of several functions for the calculation rules of FAIR Quality KPIs
"""

from functions.classes import *

def test_function():
    """Test function to check module functionality"""
    print("You called the test function.")
    
def kpi_expense(system: LegoAssembly)->float:
    """
    Calculates the total price of the car

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        total_price (float): car price in €

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_acceptance()")

    total_price = 0
    for c in system.get_component_list(-1):
        total_price += c.properties["price [Euro]"]
        total_price = round(total_price, 2)
    return total_price

def kpi_acceptance(system: LegoAssembly)->float:
    """
    Calculates the total price of the car divided by the total mass

    Args:
        system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
        customer_acceptance (float): price/performance ratio in €/g

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_acceptance()")

    total_mass = 0
    total_price = 0
    for c in system.get_component_list(-1):
        total_mass += c.properties["mass [g]"]
        total_price += c.properties["price [Euro]"]
        price_performance_ratio = round(total_price/total_mass, 2)        
    return price_performance_ratio


def kpi_availableness(system: LegoAssembly)->int:
    """
    searching longest delivery time of all parts

    Args:
    system (LegoAssembly): LegoAssembly object that represents the system

    Returns:
    delivery_time (int): longest delivery time

    Raises:
        TypeError: If an argument with unsupported type is passed
            (i.e. anything other than LegoAssembly).
    """
    if not isinstance(system, LegoAssembly):
        raise TypeError(f"Unsupported type {type(system)} passed to kpi_availableness()")
    delivery_time = 0
    for c in system.get_component_list(-1):
        if delivery_time < c.properties["delivery time [days]"]:
            delivery_time = c.properties["delivery time [days]"]
    return delivery_time